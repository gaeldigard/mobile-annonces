let containerAnnonces = jQuery('#liste-annonces')
let containerAnnoncesListe = 
jQuery(document).ready(function(){
    
    // https://api.annonces.nc/posts?by_category=sportives-1100&per=28&sort=-published_at&by_locality=nouvelle-caledonie&page=1

    jQuery.get('https://api.annonces.nc/posts?by_category=trails-1100&by_locality=nouvelle-caledonie', function(data){
        data.forEach(element => {
             
            let line = jQuery('<li style="cursor:pointer" class="list-group-item d-flex justify-content-between align-items-center" data-slug="'+element.slug+'">')
            line.html( element.title );
            if(element.price != null){
                line.append( '<span class="badge badge-primary badge-pill">'+element.price.toLocaleString('fr-FR')+'F</span>' );
            }
            jQuery('#liste-annonces ul').append(line)
        });
    });

   
jQuery('#liste-annonces ul').on('click', 'li', function (e) {
    jQuery('#liste-annonces li').removeClass('active')
    jQuery(this).addClass('active')
    let slug = jQuery(this).attr('data-slug')
    jQuery('#detailModal').modal('show')
    jQuery.get('https://api.annonces.nc/posts/'+slug, function(data){

        jQuery('#detailModal .modal-title').html(data.title)

        let desc = jQuery('<p>');
        desc.append(data.description)
        desc.append('<br/>');

        if(data.user != null){
            let contact = jQuery('<footer class="blockquote-footer">');
            if(data.user.lastname != undefined)
                contact.append(data.user.lastname + ', ')    
            if(data.user.firstname != undefined)
                contact.append(data.user.firstname + ', ')
            if(data.user.phone != undefined)
                contact.append('Tél. ' + data.user.phone + ', ')
            if(data.user.mobile != undefined)
                contact.append('Gsm. ' + data.user.mobile + ', ')

            desc.append(contact);
        }
        
        if(data.medias[0] != undefined )
            desc.append('<img src="'+data.medias[0].versions.large.url+'" width="50%" class="img-fluid" alt="">')
        if(data.medias[1] != undefined )
            desc.append('<img src="'+data.medias[1].versions.large.url+'" width="50%" class="img-fluid" alt="">')
        if(data.medias[2] != undefined )
            desc.append('<img src="'+data.medias[2].versions.large.url+'" width="50%" class="img-fluid" alt="">')
        if(data.medias[3] != undefined )
            desc.append('<img src="'+data.medias[3].versions.large.url+'" width="50%" class="img-fluid" alt="">')


        jQuery('#detailModal .modal-body').html(desc);
    });
    
  })

});
